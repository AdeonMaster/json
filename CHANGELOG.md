## Changelog

- Renamed project to `json`
- Hacked away the support for ANSI encodings in nlohmann::json (DO NOT UPDATE THE nlohmann::json dependency in the future without restoring this hack!)
- Replaced file functions with a generic ones:
  - JSON.dump_ansi
  - JSON.dump_utf8
  - JSON.parse_ansi
  - JSON.parse_utf8