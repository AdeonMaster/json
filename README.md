# json - Simple JSON utilities for Squirrel and SqRat. Created for [Gothic 2 Online](https://gothic-online.com.pl/) platform.

## Table of contents

- [Download](#download)
- [Usage](#usage)
- [Build requirements](#build-requirements)
- [Build instructions](#build-instructions)
- [Credits](#credits)

## Download

Check the [release page](https://gitlab.com/g2o/modules/json/-/releases).

## Usage
### Functions
```lua
string JSON.dump_utf8(table tbl, int indent = -1, int indent_char = ' ', bool ensure_ascii = false)
string JSON.dump_ansi(table tbl, int indent = -1, int indent_char = ' ', bool ensure_ascii = false)
```
- **_NOTE:_**  These functions differ only in the way the page encoding is handled.
- `return string` \
    Returns json string.
- `table` tbl \
    Represents the input squirrel table object.
- `int` indent \
    If `indent` is nonnegative, then array elements and object members will be pretty-printed with that indent level. An indent level of `0` will only insert newlines. `-1` (the default) selects the most compact representation.
- `int` indent_char \
    The character to use for indentation if `indent` is greater than `0`. The default is ` ` (space).
- `int` ensure_ascii \
    If `ensure_ascii` is true, all non-ASCII characters in the output are escaped with `\uXXXX` sequences, and the
    result consists of ASCII characters only.
----
```lua
table/array JSON.parse_utf8(string str)
table/array JSON.parse_ansi(string str)
```
- **_NOTE:_**  These functions differ only in the way the page encoding is handled.
- `return table/array` \
    Returns squirrel array or table object.
- `string` str \
    Represents the input json string.

### Example code:
```lua
addEventHandler("onInit", function()
{
    system("chcp 1250")
    
    local from_container = {a = 1.5, b = "zażółć gąślą jaźń", c = false, d = null, e = {f = [1, 2, [3, 4]]}}

    local json_string = JSON.dump_ansi(from_container, 2)

    local write = file("test.json", "w+")
    write.write(json_string)
    write.close()

    local read = file("test.json", "r+")
    local to_container = JSON.parse_ansi(read.read("a"))
    read.close()

    print(to_container["b"])
})
```

## Build requirements

**_NOTE:_**  Some of the requirements like _IDE_ or _compiler_ are just recommendation.

In order to compile the module, you have to meet some \
essential requirements,
depending on the target platform.

**_NOTE:_**  The project takes advantage of SqRat C++11 optimisations (SCRAT_USE_CXX11_OPTIMIZATIONS) \
 and exceptions (SCRAT_USE_EXCEPTIONS).

### Windows

- Visual Studio, 2015+ (recommended [2019 Community Edition](https://visualstudio.microsoft.com/pl/thank-you-downloading-visual-studio/?sku=Community&rel=16))
    
    Visual Studio Components
    * Windows SDK
    * one of the following toolsets, pick one: v140, v141, v142 (recommended v142)
    * (Optional) CMake Tools for Visual Studio
- [CMake 3.17+](https://cmake.org/download/)

### Linux

- g++ compiler
- [CMake 3.17+](https://cmake.org/download/)

## Build instructions

### Windows

#### Visual Studio with CMake tools

- open a local folder using Visual Studio
- build the project

#### Visual Studio without CMake tools

- open command line in repo-directory
- type ``mkdir build``
- type ``cd build``
- type ``cmake ..``
- open visual studio .sln and compile the project
- alternatively if you want to build from command line instead, \
    type ``cmake --build .``

### Linux

- open terminal in repo-directory
- type ``mkdir build``
- type ``cd build``
- type ``cmake ..``
- type ``cmake --build .``

### Credits
- [Nlohmann JSON project](https://github.com/nlohmann/json)
- Patrix - Refactor and rewrite
- Shoun - Initial release
