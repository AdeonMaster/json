#include "json.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

static void set_json_object(json& jsonObject, Sqrat::Table& sqTable);
static void set_json_array(json& jsonArray, Sqrat::Array& sqArray);

static void set_sq_table(json& jsonObject, Sqrat::Table& sqTable);
static void set_sq_array(json& jsonArray, Sqrat::Array& sqArray);

nlohmann::json_abi_v3_11_2::detail::error_handler_t dump_error_handler;

SQInteger JSON::dump_utf8(HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 5)
		return sq_throwerror(vm, "wrong number of parameters");

	Sqrat::Var<Sqrat::Table> table(vm, 2);

	SQInteger indent = -1;
	if (top >= 3)
		sq_getinteger(vm, 3, &indent);

	SQInteger indent_char = 32;
	if (top >= 4)
		sq_getinteger(vm, 4, &indent_char);

	SQBool ensure_ascii = false;
	if (top == 5)
		sq_getbool(vm, 5, &ensure_ascii);

	json json;
	set_json_object(json, table.value);

	try
	{
		Sqrat::string dump = json.dump(indent, indent_char, ensure_ascii, dump_error_handler);
		sq_pushstring(vm, dump.c_str(), -1);
	}
	catch (json::type_error& ex)
	{
		std::string errorMsg = ex.what();
		size_t errorMsgBeginIdx = errorMsg.find("] ");

		return sq_throwerror(vm, errorMsg.c_str() + ((errorMsgBeginIdx != std::string::npos) ? errorMsgBeginIdx + 2 : 0));
	}

	return 1;
}

SQInteger JSON::dump_ansi(HSQUIRRELVM vm)
{
	nlohmann::json_abi_v3_11_2::detail::use_ansi_encoding = true;
	dump_error_handler = nlohmann::json_abi_v3_11_2::detail::error_handler_t::ignore;

	SQRESULT result = dump_utf8(vm);

	nlohmann::json_abi_v3_11_2::detail::use_ansi_encoding = false;
	dump_error_handler = nlohmann::json_abi_v3_11_2::detail::error_handler_t::strict;

	return result;
}

SQInteger JSON::parse_utf8(HSQUIRRELVM vm)
{
	const SQChar* text;
	sq_getstring(vm, 2, &text);

	json json;

	try
	{
		json = json::parse(text);
	}
	catch (json::parse_error& ex)
	{
		std::string errorMsg = ex.what();
		size_t errorMsgBeginIdx = errorMsg.find("] ");

		return sq_throwerror(vm, errorMsg.c_str() + ((errorMsgBeginIdx != std::string::npos) ? errorMsgBeginIdx + 2 : 0));
	}

	Sqrat::Table table(vm);
	set_sq_table(json, table);
	
	sq_pushobject(vm, table.GetObject());
	return 1;
}

SQInteger JSON::parse_ansi(HSQUIRRELVM vm)
{
	nlohmann::json_abi_v3_11_2::detail::use_ansi_encoding = true;
	SQRESULT result = parse_utf8(vm);
	nlohmann::json_abi_v3_11_2::detail::use_ansi_encoding = false;

	return result;
}

static void set_json_object(json& jsonObject, Sqrat::Table& sqTable)
{
	Sqrat::Object::iterator it;

	while (sqTable.Next(it))
	{
		HSQOBJECT idx = it.getKey();
		HSQOBJECT val = it.getValue();

		const SQChar* index = it.getName();
		if (index == nullptr)
			return;

		switch (val._type)
		{
			case OT_NULL:
			{
				jsonObject[index] = nullptr;
				break;
			}

			case OT_BOOL:
			{
				jsonObject[index] = val._unVal.nInteger == 1;
				break;
			}

			case OT_INTEGER:
			{
				jsonObject[index] = val._unVal.nInteger;
				break;
			}

			case OT_FLOAT:
			{
				jsonObject[index] = val._unVal.fFloat;
				break;
			}

			case OT_STRING:
			{
				jsonObject[index] = sq_objtostring(&val);
				break;
			}

			case OT_ARRAY:
			{
				json nestedJsonArray = json::array();
				Sqrat::Array nestedSqArray(val);

				set_json_array(nestedJsonArray, nestedSqArray);
				jsonObject[index] = nestedJsonArray;
				break;
			}

			case OT_TABLE:
			{
				json nestedJsonObject = json::object();
				Sqrat::Table nestedSqTable(val);

				set_json_object(nestedJsonObject, nestedSqTable);
				jsonObject[index] = nestedJsonObject;
				break;
			}
		}	
	}
}

static void set_json_array(json& jsonObject, Sqrat::Array& sqArray)
{
	Sqrat::Object::iterator it;

	while (sqArray.Next(it))
	{
		HSQOBJECT idx = it.getKey();
		HSQOBJECT val = it.getValue();

		SQInteger index = idx._unVal.nInteger;

		switch (val._type)
		{
		case OT_NULL:
		{
			jsonObject[index] = nullptr;
			break;
		}

		case OT_BOOL:
		{
			jsonObject[index] = val._unVal.nInteger == 1;
			break;
		}

		case OT_INTEGER:
		{
			jsonObject[index] = val._unVal.nInteger;
			break;
		}

		case OT_FLOAT:
		{
			jsonObject[index] = val._unVal.fFloat;
			break;
		}

		case OT_STRING:
		{
			jsonObject[index] = sq_objtostring(&val);
			break;
		}

		case OT_ARRAY:
		{
			json nestedJsonArray = json::array();
			Sqrat::Array nestedSqArray(val);

			set_json_array(nestedJsonArray, nestedSqArray);
			jsonObject[index] = nestedJsonArray;
			break;
		}

		case OT_TABLE:
		{
			json nestedJsonObject = json::object();
			Sqrat::Table nestedSqTable(val);

			set_json_object(nestedJsonObject, nestedSqTable);
			jsonObject[index] = nestedJsonObject;
			break;
		}
		}
	}
}

static void set_sq_array(json& jsonArray, Sqrat::Array& sqArray)
{
	using namespace SqModule;

	for (auto& value : jsonArray)
	{
		switch (value.type())
		{
		case json::value_t::null:
			sqArray.Append(nullptr);
			break;

		case json::value_t::boolean:
			sqArray.Append(value.get<bool>());
			break;

		case json::value_t::number_integer:
		case json::value_t::number_unsigned:
			sqArray.Append(value.get<SQInteger>());
			break;

		case json::value_t::number_float:
			sqArray.Append(value.get<SQFloat>());
			break;

		case json::value_t::string:
			sqArray.Append(value.get<Sqrat::string>());
			break;

		case json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			set_sq_array(value, nestedSqArray);
			sqArray.Append(nestedSqArray);
			break;
		}

		case json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			set_sq_table(value, nestedSqTable);
			sqArray.Append(nestedSqTable);
			break;
		}
		}
	}
}

static void set_sq_table(json& jsonObject, Sqrat::Table& sqTable)
{
	using namespace SqModule;

	for (auto& el : jsonObject.items())
	{
		auto& key = el.key();
		auto& value = el.value();

		switch (el.value().type())
		{
		case json::value_t::null:
			sqTable.SetValue(key.c_str(), nullptr);
			break;

		case json::value_t::boolean:
			sqTable.SetValue(key.c_str(), value.get<bool>());
			break;

		case json::value_t::number_integer:
		case json::value_t::number_unsigned:
			sqTable.SetValue(key.c_str(), value.get<SQInteger>());
			break;

		case json::value_t::number_float:
			sqTable.SetValue(key.c_str(), value.get<SQFloat>());
			break;

		case json::value_t::string:
			sqTable.SetValue(key.c_str(), value.get<Sqrat::string>());
			break;

		case json::value_t::array:
		{
			Sqrat::Array nestedSqArray(vm);

			set_sq_array(value, nestedSqArray);
			sqTable.SetValue(key.c_str(), nestedSqArray);
			break;
		}

		case json::value_t::object:
		{
			Sqrat::Table nestedSqTable(vm);

			set_sq_table(value, nestedSqTable);
			sqTable.SetValue(key.c_str(), nestedSqTable);
			break;
		}
		}
	}
}