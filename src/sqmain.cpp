#include <sqrat.h>
#include "json.h"

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);
	Sqrat::DefaultVM::Set(vm);

	Sqrat::RootTable(vm).Bind("JSON", Sqrat::Table(vm)
		.SquirrelFunc("dump_utf8", &JSON::dump_utf8, -2, ".tiib")
		.SquirrelFunc("dump_ansi", &JSON::dump_ansi, -2, ".tiib")
		.SquirrelFunc("parse_utf8", &JSON::parse_utf8, 2, ".s")
		.SquirrelFunc("parse_ansi", &JSON::parse_ansi, 2, ".s")
	);

	return SQ_OK;
}
