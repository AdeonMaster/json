#pragma once

#include <sqrat.h>

namespace JSON
{
	SQInteger dump_utf8(HSQUIRRELVM vm);
	SQInteger dump_ansi(HSQUIRRELVM vm);

	SQInteger parse_utf8(HSQUIRRELVM vm);
	SQInteger parse_ansi(HSQUIRRELVM vm);
}
